'use strict'

const port = process.env.PORT || 3000;

const https = require ('https');
const express = require ('express'); //utiliza ya mongodb
const helmet = require("helmet");
const logger = require ('morgan');
const mongojs = require ('mongojs');
const token = require ('./services/token.service');

const URL_DB="mongodb+srv://sofia:universidad18@cluster0.yljbb.mongodb.net/vehiculos?retryWrites=true&w=majority"

const fs = require ('fs');

const app = express ();
app.use(helmet());
const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

var db = mongojs(URL_DB); //enlazado con la db SD
var id = mongojs.ObjectID;//Función para convertir un id textual en un objeto mongojs

//Declaramos nuestros Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false})); //leer
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) => {
    console.log('param /api/:colecciones/');
    console.log('colección: ',coleccion);

    req.collection = db.collection(coleccion);//Creamos un puntero que apunta a la DB y a la tabla (coleccion) indicadas
    return next();
});
function auth (req,res,next){
    if(!req.headers.authorization){
        res.status(403).json( {
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera Authorization."

        });
        return next(new Error("Falta token de autorizacion"));
    }
    
    
    //console.log(req.headers.authorization);
    if(token.decodificarToken(req.headers.authorization.split(" ")[1])){
        return next();
    }

   
    
    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio"
    });
    return next(new Error ("Acceso no autorizado"));
}
//Rutas y Controladores
app.get('/api/', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);
    db.getCollectionNames((err,colecciones) =>{
        if(err) return next(err);
        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });

    });


});

app.get('/api/:colecciones', (req, res, next) => { 
    console.log('GET /api/:colecciones');
    console.log(req.params);
    console.log(req.collection);
    if(JSON.stringify(req.query) != '{}'){ 
        
        req.collection.find({Estado: "libre", color: req.query.color ,Plazas: req.query.Plazas}, (err, elemento) => {
            if(elemento){
                
                    if(err) return next(err);
                    console.log(elemento);
                    res.json({
                        result: 'OK',
                        colección: req.params.colecciones,
                        elementos: elemento
                    });
                

            }else{
                res.status(401).json({
                    result: 'No existen'
                });
            }

        });

   }else{

       req.collection.find((err,coleccion)=> {
            if(err) return next(err);
            console.log(coleccion);
            res.json({
                result: 'OK',
                colección: req.params.colecciones,
                elementos: coleccion
            });
        });
    }  
    
});
app.get('/api/:colecciones/:id', (req, res, next) => { //por id
    const queId=req.params.id; 
    const queColeccion=req.params.colecciones;

    req.collection.findOne({_id: id(queId)}, (err, elemento) => { 
        if (err) return next(err); 
        //console.log(elemento);
        res.json({
            result:'OK',
            colección: queColeccion,
            elementos : elemento
        }); 
    }); 
});
app.get('/api/:colecciones/:id', (req, res, next) => { //por id
    const queId=req.params.id; 
    const queColeccion=req.params.colecciones;

    req.collection.findOne({_id: id(queId)}, (err, elemento) => { 
        if (err) return next(err); 
        //console.log(elemento);
        res.json({
            result:'OK',
            colección: queColeccion,
            elementos : elemento
        }); 
    }); 
});
app.post ('/api/:colecciones', (req, res, next) => {
    const nuevoElemento=req.body;
    req.collection.save(nuevoElemento,(err,elementoGuardado)=>{
        if(err) return next(err);

        res.status(201).json({
            result: 'OK',
            colección: req.params.colecciones,
            elemento: elementoGuardado
        });
    });
});

app.put('/api/:colecciones/:id', (req, res, next) => {
    const elementoId = req.params.id;
    const queColeccion= req.params.colecciones;
    req.collection.update({_id: id(elementoId)}, {$set: req.body},
    {safe: true, multi: false}, (err, result) => {
    if (err) return next(err);
    res.json({
        result: 'OK',
        colección: queColeccion,
        _id: elementoId,
        resultado : result
        });
    });
});
app.put('/api/:colecciones/:id/reserva', auth, (req, res, next) => {
    const nuevosDatos={Estado:'reservado'}
    //nuevosDatos.estado = 'reservado';
    const queColeccion= req.params.colecciones;
    token.decodificarToken(req.headers.authorization.split(" ")[1])

    .then(id_user => {
        req.collection.update({_id: id(req.params.id)}, {$set: nuevosDatos},{safe: true, multi: false}, (err, result) =>{
            if (err) return next(err);
            res.json({
                result: 'OK',
                colección: queColeccion,
                _id: req.params.id,
                resultado : result
            });
            

        });
        
    });
  
});
app.put('/api/:colecciones/:id/cancelar', auth, (req, res, next) => {
    const nuevosDatos={Estado:'libre'}
   
    const queColeccion= req.params.colecciones;
    token.decodificarToken(req.headers.authorization.split(" ")[1])

    .then(id_user => {
        req.collection.update({_id: id(req.params.id)}, {$set: nuevosDatos},{safe: true, multi: false}, (err, result) =>{
            if (err) return next(err);
            res.json({
                result: 'OK',
                colección: queColeccion,
                _id: req.params.id,
                resultado : result
            });
            

        });
        
    });
  
});
app.delete('/api/:colecciones/:id', (req, res, next) => {
    let elementoId = req.params.id;
    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
    if (err) return next(err);
    res.json(resultado);
    });
    });
https.createServer(opciones,app).listen(port, ()=> {
        console.log(`API GW RESTFull CRUD ejecutándose en https://localhost:${port}/api/{colecciones}/{id}`);
});
/*app.listen(port, () => {
    console.log(`API RESTFull CRUD ejecutándose en http://localhost:${port}/api/{colecciones}/{id}`);
});*/


