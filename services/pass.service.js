'use strict'
const bcrypt=require ('bcrypt');
//Devuelve un hash con salt incluido con formato//
//Salt1:  $2b$10$bTOU/.gEH3fjCaYqbAuv.O
//Hash1:  $2b$10$bTOU/.gEH3fjCaYqbAuv.OujQlKlg1YwNmb7XI2jVA5uMR7dJXZgm

function encriptaPassword(password){
    return bcrypt.hash(password, 10);
}
//Devulve true o false 

function compararPassword(password,hash){
    return bcrypt.compare(password,hash);
}
module.exports ={
    encriptaPassword,
    compararPassword
};
//Encripta

/*//Importamos módulos de criptografía
const bcrypt=require ('bcrypt');
//Simulamos datos
const miPass = 'miContraseña';
const basPass = 'otropassword';

//Creamos un salto
bcrypt.genSalt(10,(err,salt)=>{
    console.log(`Salt1:  ${salt}`);
    //Utilizamos el salt para generar un hush para el password
    bcrypt.hash(miPass,salt,(err,hash)=>{
        if(err) console.log(err);
        else console.log(`Hash1:  ${hash}`);
    });
});*/
